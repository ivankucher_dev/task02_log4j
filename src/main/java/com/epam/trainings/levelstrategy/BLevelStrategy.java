package com.epam.trainings.levelstrategy;

import com.epam.trainings.model.Model;
import static com.epam.trainings.constants.UIConstant.*;
import static com.epam.trainings.utils.PanelWorker.setImagesForLevel;
import static com.epam.trainings.constants.BallConst.*;
import static com.epam.trainings.constants.BoardConst.*;
import static com.epam.trainings.constants.BlockConst.*;

/**
 * BLevelStrategy class of strategy pattern of level 2.
 * Main purpose is to set images , sizes , width and
 * height of game models. Will be in use if user pick level2 button on
 * {@link com.epam.trainings.view.MenuPanel} .
 * Using in
 * {@link com.epam.trainings.controller.viewcontroller.startmenucontroller.ViewControllerImpl}.
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class BLevelStrategy implements LevelStrategy {
  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelImages(Model model) {
    setImagesForLevel(model, BALL_IMG_LV_2, BOARD_IMG_LV_2, BLOCK_IMG_LV_2, this);
  }
  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelBallSize(Model model) {
    model.ball.setBallLevelSize(BALL_LV_2_SIZE);
    model.ball.setDirX(BALL_DIR_X_LV_2);
    model.ball.setDirY(BALL_DIR_Y_LV_2);
  }
  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelBoardSize(Model model) {
    model.board.setBoardLevelWidth(BOARD_LV_2_WIDTH);
    model.board.setBoardLevelHeight(BOARD_LV_2_HEIGHT);
    model.board.setBoardSpeed(MOVE_BOARD_LV_2);
  }
  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelBLocks(Model model) {
    model.initializeBlocks(BLOCK_LEVEL2_WIDTH, BLOCK_LEVEL2_HEIGHT);
  }
  /** @see LevelStrategy for more information about this method */
  @Override
  public void setScoreBoardMessage(Model model) {
    model.scoreBoard.setWinMessage(WIN_MESSAGE_LV_2);
    model.scoreBoard.setLoseMessage(LOSE_MESSAGE_LV_2);
  }
}
