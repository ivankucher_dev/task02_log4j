package com.epam.trainings.levelstrategy;

import com.epam.trainings.model.Model;
import static com.epam.trainings.constants.BallConst.*;
import static com.epam.trainings.constants.UIConstant.*;
import static com.epam.trainings.constants.BoardConst.*;
import static com.epam.trainings.constants.BlockConst.*;
import static com.epam.trainings.utils.PanelWorker.*;
/**
 * ALevelStrategy class of strategy pattern of level 1.
 * Main purpose is to set images , sizes , width and
 * height of game models. Will be in use if user pick
 * level1 button on {@link com.epam.trainings.view.MenuPanel} .
 * Using in
 * {@link com.epam.trainings.controller.viewcontroller.startmenucontroller.ViewControllerImpl}.
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class ALevelStrategy implements LevelStrategy {

  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelImages(Model model) {
    setImagesForLevel(model, BALL_IMG_LV_1, BOARD_IMG_LV_1, BLOCK_IMG_LV_1, this);
  }

  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelBallSize(Model model) {
    model.ball.setBallLevelSize(BALL_LV_1_SIZE);
    model.ball.setDirX(-BALL_DIR_X_LV_1);
    model.ball.setDirY(BALL_DIR_Y_LV_1);
  }

  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelBoardSize(Model model) {
    model.board.setBoardLevelWidth(BOARD_LV_1_WIDTH);
    model.board.setBoardLevelHeight(BOARD_LV_1_HEIGHT);
    model.board.setBoardSpeed(MOVE_BOARD_LV_1);
  }
  /** @see LevelStrategy for more information about this method */
  @Override
  public void setLevelBLocks(Model model) {
    model.initializeBlocks(BLOCK_LEVEL1_WIDTH, BLOCK_LEVEL1_HEIGHT);
  }
  /** @see LevelStrategy for more information about this method */
  @Override
  public void setScoreBoardMessage(Model model) {
    model.scoreBoard.setWinMessage(WIN_MESSAGE_LV_1);
    model.scoreBoard.setLoseMessage(LOSE_MESSAGE_LV_1);
  }
}
