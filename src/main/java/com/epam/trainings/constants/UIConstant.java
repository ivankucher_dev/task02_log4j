package com.epam.trainings.constants;
/**
 * UI constants class using for define all ui constants including images path , string constants ,
 * frame width and height.
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class UIConstant {
  public static final int FRAME_WIDTH = 800;
  public static final int FRAME_HEIGHT = 700;
  public static final String START_BUTTON_TITLE = "Start game";
  public static final String SETTINGS_BUTTON_TITLE = "Settings";
  public static final String QUIT_BUTTON_TITLE = "Quit";
  public static final int START_LEVEL_VALUE = 1;
  public static final String LEVEL_TITLE = "Level";
  public static final String BALL_IMG_LV_1 = "images/level1/ball_lv_1.png";
  public static final String BALL_IMG_LV_2 = "images/level2/ball_lv_2.png";
  public static final String BALL_IMG_LV_3 = "images/level3/ball_lv_3.png";
  public static final String BOARD_IMG_LV_1 = "images/level1/board_lv_1.png";
  public static final String BOARD_IMG_LV_2 = "images/level2/board_lv_2.png";
  public static final String BOARD_IMG_LV_3 = "images/level3/board_lv_3.png";
  public static final String BLOCK_IMG_LV_1 = "images/level1/block_lv_1.jpg";
  public static final String BLOCK_IMG_LV_2 = "images/level2/block_lv_2.jpg";
  public static final String BLOCK_IMG_LV_3 = "images/level3/block_lv_3.png";
  public static final String MUSIC_BACKGROUND = "music/background.wav";
  public static final String MUSIC_WIN = "music/winGame.wav";
  public static final String MUSIC_LOSE = "music/lose.wav";
  public static final String MUSIC_JUMP = "music/jump.wav";
  public static final String MUSIC_BLOCK_CRASHED = "music/crashed.wav";
  public static final String APP_ICON = "images/icon.png";
  public static final String SOUND_BUTTON = "images/soundMuteButton.png";
  public static final String WIN_MESSAGE_LV_1 =
      "Congratz, [Far cry] didnt crash , you have destroyed all blocks.";
  public static final String LOSE_MESSAGE_LV_1 = "Oops, [Far cry] crashed, try better next time.";
  public static final String WIN_MESSAGE_LV_2 =
      "Congratz, [FIFA 18] didnt crash , you have destroyed all blocks.";
  public static final String LOSE_MESSAGE_LV_2 = "Oops, [FIFA 18] crashed, try better next time.";
  public static final String WIN_MESSAGE_LV_3 =
      "Congratz, [Dota 2] didnt crash , you have destroyed all blocks.";
  public static final String LOSE_MESSAGE_LV_3 = "Oops, [Dota 2] crashed, try better next time.";
  public static final String BACKGROUND_IMAGE_PATH = "src/main/resources/images/background.jpg";
  public static final String GAME_TITLE = "BREAK IT";
  public static final int TITLE_GAP_SIZE = 70;
}
