package com.epam.trainings.constants;
/**
 * Board constants class using for define constants of board model
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class BoardConst {
  public static final int BOARD_LV_1_WIDTH = 140;
  public static final int BOARD_LV_1_HEIGHT = 90;
  public static final int BOARD_LV_2_WIDTH = 100;
  public static final int BOARD_LV_2_HEIGHT = 100;
  public static final int BOARD_LV_3_WIDTH = 80;
  public static final int BOARD_LV_3_HEIGHT = 80;
  public static final int BOARD_POS_X = 320;
  public static final int BOARD_POS_Y = 550;
  public static final int MOVE_BOARD_LV_1 = 5;
  public static final int MOVE_BOARD_LV_2 = 7;
  public static final int MOVE_BOARD_LV_3 = 9;
}
