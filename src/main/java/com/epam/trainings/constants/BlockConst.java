package com.epam.trainings.constants;
/**
 * Block constants class using for define constants of block model
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class BlockConst {
  public static final int DISTANCE_BEETWEEN_BLOCKS = 5;
  public static final int START_POS_Y_FOR_BLOCK_INIT = 70;
  public static final int BLOCK_LEVEL1_WIDTH = 90;
  public static final int BLOCK_LEVEL1_HEIGHT = BLOCK_LEVEL1_WIDTH / 2;
  public static final int BLOCK_LEVEL2_WIDTH = 80;
  public static final int BLOCK_LEVEL2_HEIGHT = BLOCK_LEVEL2_WIDTH / 2;
  public static final int BLOCK_LEVEL3_WIDTH = 70;
  public static final int BLOCK_LEVEL3_HEIGHT = BLOCK_LEVEL3_WIDTH / 2;
  public static final int NUMBER_OF_BLOCKS_ACROSS = 7;
  public static final int NUMBER_OF_BLOCKS_IN_HEIGHT = 3;
  public static final int MAX_NUMBER_OF_HITS_TO_DESTROY_BLOCK = 3;
}
