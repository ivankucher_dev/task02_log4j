package com.epam.trainings.model.modelClasess;

import com.epam.trainings.constants.UIConstant;
import java.awt.*;
import java.awt.image.BufferedImage;
import static com.epam.trainings.constants.BoardConst.*;

/**
 * class that represents the board
 * @author epam_6th_team_lab24
 */
public class Board extends Rectangle {
  private int posX;
  private int posY;
  private int boardLevelWidth;
  private int boardLevelHeight;
  private int boardSpeed;
  private BufferedImage image;

  public Board(int x, int y, int w, int h) {
    posX = BOARD_POS_X;
    posY = BOARD_POS_Y;
    boardLevelWidth = w;
    boardLevelHeight = h;
  }

  public Rectangle getBounds() {
    return new Rectangle(posX, posY, boardLevelWidth, boardLevelHeight);
  }

  public int getBoardSpeed() {
    return boardSpeed;
  }

  public void setBoardSpeed(int boardSpeed) {
    this.boardSpeed = boardSpeed;
  }


  public int getPosY() {
    return posY;
  }

  public int getCornerPosY() {
    return posY;
  }


  public void setBoardLevelWidth(int boardLevelWidth) {
    this.boardLevelWidth = boardLevelWidth;
  }

  public void setBoardLevelHeight(int boardLevelHeight) {
    this.boardLevelHeight = boardLevelHeight;
  }

  public void setImage(BufferedImage image) {
    this.image = image;
  }

  public int getPosX() {
    return posX;
  }

  public void setPosX(int posX) {
    if (!isOutOfBounds()) {
      this.posX = posX;
      this.x = posX;
    }
  }

  /**
   * check whether board position is out of frame
   * @return true if board is out of frame
   */
  public boolean isOutOfBounds() {
    if (posX + boardLevelWidth > UIConstant.FRAME_WIDTH) {
      posX = UIConstant.FRAME_WIDTH - boardLevelWidth;
      return true;
    } else if (posX < 0) {
      posX = 0;
      return true;
    } else {
      return false;
    }
  }

  public void draw(Graphics g) {
    Graphics2D myG = (Graphics2D) g;
    myG.drawImage(image, posX, posY, boardLevelWidth, boardLevelHeight, null);
    myG.fill(this);
  }

  @Override
  public String toString() {
    return "Board [x = "
        + getPosX()
        + " , y = "
        + getPosY()
        + ", width = "
        + boardLevelWidth
        + " , height = "
        + boardLevelHeight
        + "]";
  }
}
