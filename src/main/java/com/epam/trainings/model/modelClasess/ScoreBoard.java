package com.epam.trainings.model.modelClasess;

/**
 * class that represents the score board
 * @author epam_6th_team_lab24
 */
public class ScoreBoard {
  private static final int SCORE_INCREASE_VALUE = 1;
  private String WIN_MESSAGE;
  private String LOSE_MESSAGE;
  private int score;

  public  String getWinMessage() {
    return WIN_MESSAGE;
  }

  public void setWinMessage(String winMessage) {
    WIN_MESSAGE = winMessage;
  }

  public String getLoseMessage() {
    return LOSE_MESSAGE;
  }

  public void setLoseMessage(String loseMessage) {
    LOSE_MESSAGE = loseMessage;
  }

  public ScoreBoard() {
    this.score = 0;
  }

  public void increaseScore() {
    this.score += SCORE_INCREASE_VALUE;
  }

  public int getScore() {
    return score;
  }
}
