package com.epam.trainings.model.modelClasess;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import static com.epam.trainings.constants.BallConst.*;

/**
 * class that represents the ball
 * @author epam_6th_team_lab24
 */
public class Ball extends Ellipse2D.Double {
  private int posX;
  private int posY;
  private int dirX;
  private int dirY;
  private int ballLevelSize;
  public BufferedImage image;

  public Ball(double x, double y, double radius) {
    super(x, y, radius * 2, radius * 2);
    posX =(int) x;
    posY = (int) y;
    dirX = BALL_DIR_X;
    dirY = BALL_DIR_Y;
  }

  public Rectangle getBounds() {
    return new Rectangle(posX, posY, ballLevelSize, ballLevelSize);
  }

  public int getDirX() {
    return dirX;
  }

  public int getDirY() {
    return dirY;
  }

  public void setDirY(int dirY) {
    this.dirY = dirY;
  }

  public void setPosX(int posX) {
    this.posX = posX;
    this.x = posX;
  }

  public void setPosY(int posY) {
    this.posY = posY;
    this.y = posY;
  }

  public void setDirX(int dirX) {
    this.dirX = dirX;
  }

  public int getBottomPosY() {
    return posY + ballLevelSize - 1;
  }

  public void setBallLevelSize(int ballLevelSize) {
    this.ballLevelSize = ballLevelSize;
  }

  public int getBallLevelSize() {
    return ballLevelSize;
  }

  public void setImage(BufferedImage image) {
    this.image = image;
  }

  public void draw(Graphics g) {
    Graphics2D myG = (Graphics2D) g;
    myG.drawImage(image, posX, posY, ballLevelSize, ballLevelSize, null);
    myG.fill(this);
  }

  @Override
  public String toString() {
    return "Ball [x = "
            + getX()
            + " , y = "
            + getY()
            + ", width = "
            + getWidth()
            + " , height = "
            + getHeight()
            + "]";
  }
}
