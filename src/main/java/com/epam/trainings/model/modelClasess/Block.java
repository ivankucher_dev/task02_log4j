package com.epam.trainings.model.modelClasess;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * class that represents the block
 * @author epam_6th_team_lab24
 */
public class Block extends Rectangle2D.Double {

  private boolean destroyed;
  private int numberOfHitsToDestroy;
  private BufferedImage image;

  public Block(int posX, int posY, int height, int width, int numberOfHitsToDestroy) {
    super(posX, posY, width, height);
    this.numberOfHitsToDestroy = numberOfHitsToDestroy;
    this.destroyed = false;
  }

  /**
   * This method calls when ball hits the block
   * and checks whether block is destroyed or no
   */
  public void hit() {
    this.numberOfHitsToDestroy--;
    if (this.numberOfHitsToDestroy <= 0) {
      this.destroyed = true;
    }
  }

  public void setImage(BufferedImage image) {
    this.image = image;
  }

  public boolean isDestroyed() {
    return this.destroyed;
  }

  public void draw(Graphics g) {
    Graphics2D myG = (Graphics2D) g;
    myG.drawImage(
        image,
        (int) this.getX(),
        (int) this.getY(),
        (int) this.getWidth(),
        (int) this.getHeight(),
        null);
  }

  @Override
  public String toString() {
    return "Block [x = "
        + getX()
        + " , y = "
        + getY()
        + ", width = "
        + getWidth()
        + " , height = "
        + getHeight()
        + "]";
  }
}
