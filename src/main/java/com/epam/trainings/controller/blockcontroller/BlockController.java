package com.epam.trainings.controller.blockcontroller;
/**
 * BlockController Interface which provides main methods to control model of Block
 * @see com.epam.trainings.model.modelClasess.Block
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public interface BlockController {
  /**
   * Business logic for Block model . Check block for hit by ball and deleting it
   * from a frame , if number of hits of block equals 0.
   * @see com.epam.trainings.controller.viewcontroller.gameviewcontroller.GameViewControllerImpl
   */
  void checkForHit();
}
