package com.epam.trainings.controller.viewcontroller.gameovercontroller;

import com.epam.trainings.main.StartGame;
import com.epam.trainings.model.Model;
import com.epam.trainings.view.GameOverPanel;
/**
 * Controller of {@link GameOverPanel}
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class GameOverControllerImpl implements GameOverController {
  /** View which is controlled by this controller */
  private GameOverPanel view;
  /** Field of model , need to access other models to update them */
  private Model model;
  /**
   * Constructor for class GameOverController
   *
   * @param model inject model in our class
   * @param view inject view in our class
   */
  public GameOverControllerImpl(Model model, GameOverPanel view) {
    this.view = view;
    this.model = model;
    view.setGameOverController(this);
  }
  /** Main purpose is to get ready model and view to restart */
  @Override
  public void recreateGame() {
    model.recreateGameForNew();
    view = new GameOverPanel();
  }
}
