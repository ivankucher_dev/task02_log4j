package com.epam.trainings.controller.viewcontroller.settingsviewcontroller;

import com.epam.trainings.constants.Const;
import com.epam.trainings.model.Model;
import com.epam.trainings.view.SettingsPanel;
import static com.epam.trainings.constants.Const.*;
/**
 * Controller of {@link com.epam.trainings.view.SettingsPanel} view
 *
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class SettingsViewControllerImpl implements SettingsViewController {
  /** Field of model , need to access other models to update them */
  Model model;
  /** View which is controlled by this controller */
  SettingsPanel view;
  /**
   * Constructor to set model and view ,and to inject controller in our view
   *
   * @param model inject model in our class
   * @param view inject view in our class
   */
  public SettingsViewControllerImpl(Model model, SettingsPanel view) {
    this.model = model;
    this.view = view;
    view.setViewController(this);
  }
  /**
   * Method which takes fps as parametr from view and depends on it changing fps of our game by
   * timer delay
   *
   * @param pick get picked radio button from view
   */
  @Override
  public void changeFps(int pick) {
    switch (pick) {
      case FPS_HIGH_PICK:
        Const.TIMER_DELAY = 5;
        break;

      case FPS_MEDIUM_PICK:
        Const.TIMER_DELAY = 15;
        break;
      case FPS_LOW_PICK:
        Const.TIMER_DELAY = 120;
        break;

      default:
        System.err.println("Must be just 3 level of fps");
        System.exit(1);
    }
  }
}
