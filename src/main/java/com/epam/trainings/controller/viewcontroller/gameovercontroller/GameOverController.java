package com.epam.trainings.controller.viewcontroller.gameovercontroller;
/**
 * GameOverController Interface which provides main methods to work with
 * {@link com.epam.trainings.view.GameOverPanel} view
 * @see com.epam.trainings.controller.viewcontroller.gameviewcontroller.GameViewControllerImpl
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public interface GameOverController {
  /**
   * Nullifying some objects , variables and recreating objects to make
   * them ready for restart.
   */
  void recreateGame();
}
