package com.epam.trainings.controller.viewcontroller.startmenucontroller;

import com.epam.trainings.levelstrategy.ALevelStrategy;
import com.epam.trainings.levelstrategy.BLevelStrategy;
import com.epam.trainings.levelstrategy.CLevelStrategy;
import com.epam.trainings.levelstrategy.LevelContext;
import com.epam.trainings.model.Model;
import com.epam.trainings.utils.PanelWorker;
import com.epam.trainings.view.GamePanel;
import com.epam.trainings.view.MenuPanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import static com.epam.trainings.utils.SoundsWorker.*;
import static com.epam.trainings.constants.UIConstant.MUSIC_BACKGROUND;
/**
 * ViewController implemention which provides main methods to work with MenuPanel view
 * {@link com.epam.trainings.view.MenuPanel} view
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public class ViewControllerImpl implements ViewController {
  /** Field of model , need to access other models to update them */
  private Model model;
  /** View which is controlled by this controller */
  private MenuPanel view;
  /** Context for level strategy pattern */
  private LevelContext levelContext;
  private static Logger logger1 = LogManager.getLogger(ViewControllerImpl.class.getName());
  /**
   * Constructor to set model and view ,and to inject controller in our view
   * @param model inject model in our class
   * @param view inject view in our class
   */
  public ViewControllerImpl(Model model, MenuPanel view) {
    this.model = model;
    this.view = view;
    this.levelContext = new LevelContext();
    view.setViewController(this);
    logger1.info("Controller setted");
    logger1.warn("Controller setted, but mvc pattern is not complicated");
  }
  /**
   * Main method to realize business logic for level pick
   * @param level picked value of radio button from view
   */
  public void levelPicked(int level) {
    if (level == 1) {
      customizeLevel1();
    } else if (level == 2) {
      customizeLevel2();
    } else {
      customizeLevel3();
    }
  }
  /**
   * Submethod of levelPicked method to get ready all components for
   * concrete level ( 1 level) using strategy pattern.
   */
  private void customizeLevel1() {
    levelContext.setLevelStrategy(new ALevelStrategy());
    levelContext.initBlocks(model);
    levelContext.loadImagesForLevel(model);
    levelContext.setLevelBallSize(model);
    levelContext.setLevelBoardSize(model);
    levelContext.setScoreBoardMessage(model);
    logger1.info("Customize level 1");
  }
  /**
   * Submethod of levelPicked method to get ready all components for
   * concrete level ( 2 level) using strategy pattern.
   */
  private void customizeLevel2() {
    levelContext.setLevelStrategy(new BLevelStrategy());
    levelContext.initBlocks(model);
    levelContext.loadImagesForLevel(model);
    levelContext.setLevelBallSize(model);
    levelContext.setLevelBoardSize(model);
    levelContext.setScoreBoardMessage(model);
    logger1.info("Customize level 2");
  }
  /**
   * Submethod of levelPicked method to get ready all components for
   * concrete level ( 3 level) using strategy pattern.
   */
  private void customizeLevel3() {
    levelContext.setLevelStrategy(new CLevelStrategy());
    levelContext.initBlocks(model);
    levelContext.loadImagesForLevel(model);
    levelContext.setLevelBallSize(model);
    levelContext.setLevelBoardSize(model);
    levelContext.setScoreBoardMessage(model);
    logger1.info("Customize level 3");
  }

  public void changePanel(GamePanel gamePanel , JFrame topFrame){
    soundNow.stop();
    PanelWorker.changePanel(gamePanel, topFrame);
  }

  public void playBackgroundMusic() {
    playSound(MUSIC_BACKGROUND);
  }

  @Override
  public void muteSounds() {
    muted = !muted;
    if(!muted){
      playSound(MUSIC_BACKGROUND);
      logger1.info("Sounds started");
    } else {
      soundNow.stop();
      logger1.info("Sounds stopped");
    }
  }
}
