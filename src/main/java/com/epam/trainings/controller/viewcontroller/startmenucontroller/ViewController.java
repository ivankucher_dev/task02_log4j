package com.epam.trainings.controller.viewcontroller.startmenucontroller;

import com.epam.trainings.view.GamePanel;

import javax.swing.*;

/**
 * ViewController Interface which provides main methods to work with MenuPanel
 * {@link com.epam.trainings.view.MenuPanel} view
 * @see com.epam.trainings.controller.viewcontroller.startmenucontroller.ViewControllerImpl
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public interface ViewController {
  /**
   * Taking level pick from view radio button as a parameter
   * and depends on it using level strategy to initialize model for concrete level
   * @param level picked from Menuview from radio button
   * @see com.epam.trainings.levelstrategy.LevelStrategy
   */
  void levelPicked(int level);
  /**
   * Method to switch on views
   * @param gamePanel view that we want to show
   * @param topFrame frame where we need to open it
   * @author epam_6th_team_lab24
   */
  void changePanel(GamePanel gamePanel , JFrame topFrame);
  /**
   * Method to start playing music on the menu view
   * @author epam_6th_team_lab24
   */
  void playBackgroundMusic();
  /**
   * Mute all sounds if we pick mute button
   * @author epam_6th_team_lab24
   */
  void muteSounds();
}
