package com.epam.trainings.controller.ballcontroller;
/**
 * BallController Interface which provides main methods to control model of Ball
 * @see com.epam.trainings.model.modelClasess.Ball
 * @author epam_6th_team_lab24
 * @version 1.0
 * @since 2019-08-27
 */
public interface BallController {
  /**
   * Method to move ball object on the screen by ball speed
   */
  void moveBall();
}
