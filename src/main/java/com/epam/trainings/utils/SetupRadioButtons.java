package com.epam.trainings.utils;

import javax.swing.*;
import java.util.List;

/**
 * class that sets up radio buttons
 *
 * @author epam_6th_team_lab24
 */
public class SetupRadioButtons {

  public static void setupRadioButtonsOnView(
      GroupLayout.Group verticalGroup,
      GroupLayout.Group horizontalGroup,
      List<JRadioButton> levelButtons) {
    levelButtons.forEach(
        b -> {
          verticalGroup.addComponent(b);
          horizontalGroup.addComponent(b);
        });
  }
}
