package com.epam.trainings.utils;

import com.epam.trainings.levelstrategy.LevelStrategy;
import com.epam.trainings.model.Model;
import com.epam.trainings.model.modelClasess.Block;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.IOException;
/**
 * class which interact with panels. Used to change panels and set images for model considering the
 * level
 *
 * @author epam_6th_team_lab24
 */
public class PanelWorker {
  /**
   * Method to switch on views
   *
   * @param panel view that we want to show
   * @param jFrame frame where we need to open it
   * @author epam_6th_team_lab24
   */
  public static void changePanel(JPanel panel, JFrame jFrame) {
    jFrame.getContentPane().removeAll();
    jFrame.getContentPane().add(panel);
    jFrame.revalidate();
    jFrame.repaint();
    panel.requestFocusInWindow();
  }
  /**
   * Method to setup images for our game depends on level
   *
   * @author epam_6th_team_lab24
   */
  public static void setImagesForLevel(
      Model model, String ballImage, String boardImage, String blockImage, LevelStrategy straregy) {
    try {
      model.ball.setImage(ImageIO.read(FileWorker.getFileFromURL(ballImage, straregy)));
      model.board.setImage(ImageIO.read(FileWorker.getFileFromURL(boardImage, straregy)));
      for (Block block : model.blocks) {
        block.setImage(ImageIO.read(FileWorker.getFileFromURL(blockImage, straregy)));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
